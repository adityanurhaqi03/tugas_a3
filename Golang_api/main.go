package main

import (
	"log"

	"main.go/book"

	"main.go/handler"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
)

func main() {
	dsn := "root:root@tcp(127.0.0.1:3306)/pustaka_api?charset=utf8mb4&parseTime=True&loc=Local"
	db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})
	if err != nil {
		log.Fatal("DB connection error")
	}

	db.AutoMigrate(book.Book{})

	bookRepository := book.NewRepository(db)
	bookService := book.NewService(bookRepository)
	bookHandler := handler.NewBookHandler(bookService)

	r := gin.Default()

	v1 := r.Group("v1")

	v1.GET("books", bookHandler.GetBooks)
	v1.GET("books/:id", bookHandler.GetBook)
	v1.POST("books", bookHandler.CreateBook)
	v1.PUT("books/:id", bookHandler.UpdateBook)
	v1.DELETE("books/:id", bookHandler.DeleteBook)

	r.Run()
}
